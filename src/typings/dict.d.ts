/**字段类型 */
type DictType = {
  label: string;
  value: string;
  tagClass?: string;
  tagType?: string;
};
